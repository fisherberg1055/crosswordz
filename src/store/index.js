import Vue from 'vue'
import Vuex from 'vuex'
// eslint-disable-next-line
import axios from 'axios'

Vue.use(Vuex)

const crosswordConst = {
  width: 20,
  height: 18,
  tasks: [
    { id: 1, x: 7, y: 12, answer_length: 8, answer: null, direction: 'h' },
    { id: 2, x: 8, y: 10, answer_length: 8, answer: null, direction: 'v' },
    { id: 3, x: 12, y: 4, answer_length: 12, answer: null, direction: 'v' },
    { id: 4, x: 9, y: 5, answer_length: 11, answer: null, direction: 'h' },
    { id: 5, x: 10, y: 11, answer_length: 4, answer: null, direction: 'v' },
    { id: 6, x: 15, y: 0, answer_length: 11, answer: null, direction: 'v' },
    { id: 7, x: 12, y: 0, answer_length: 6, answer: null, direction: 'h' },
    { id: 8, x: 17, y: 3, answer_length: 8, answer: null, direction: 'v' },
    { id: 9, x: 3, y: 15, answer_length: 6, answer: null, direction: 'h' },
    { id: 10, x: 4, y: 11, answer_length: 6, answer: null, direction: 'v' },
    { id: 11, x: 13, y: 2, answer_length: 3, answer: null, direction: 'h' },
    { id: 12, x: 0, y: 12, answer_length: 5, answer: null, direction: 'h' },
    { id: 13, x: 9, y: 0, answer_length: 6, answer: null, direction: 'v' },
    { id: 14, x: 8, y: 17, answer_length: 6, answer: null, direction: 'h' }
  ]
}

export default new Vuex.Store({
  state: {
    user: {
      name: null,
      email: null
    },
    tasks: [],
    started_at: null
  },
  getters: {
    crosswordDefinition: state => {
      const crossword = JSON.parse(JSON.stringify(crosswordConst))
      crossword.tasks.forEach(item => {
        const task = state.tasks.find(task => (task.id === item.id))
        item.answer = (task && task.answer) || null
      })
      return crossword
    }
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    // eslint-disable-next-line
    setTasks (state, { tasks, started_at }) {
      state.tasks = tasks
      // eslint-disable-next-line
      state.started_at = started_at
    }
  },
  actions: {
    getUser ({ commit }) {
      // todo: удалить
      commit('setUser', { name: 'Ivan', email: 'example@email.com' })

      // * конец удаления, ниже раскомментировать * //

      // axios.get('/me').then(response => {
      //   commit('setUser', response.data.user)
      // }).catch(response => {
      //    window.location.href = 'process.env.VUE_APP_GUEST_REDIRECT_URL'
      // })
    },
    getTasks ({ commit }) {
      // todo: удалить
      commit('setTasks', {
        tasks: [
          { id: 1, answer_length: 8, answer: null }, // ПОКРЫТИЕ
          { id: 2, answer_length: 8, answer: null }, // СВОЙСТВО
          { id: 3, answer_length: 12, answer: 'ПОЛЬЗОВАТЕЛЬ' }, // ПОЛЬЗОВАТЕЛЬ
          { id: 4, answer_length: 11, answer: 'АВТОРИЗАЦИЯ' }, // АВТОРИЗАЦИЯ
          { id: 5, answer_length: 4, answer: null }, // ПРОД
          { id: 6, answer_length: 11, answer: 'НАГРУЗОЧНОЕ' }, // НАГРУЗОЧНОЕ
          { id: 7, answer_length: 6, answer: null }, // ЧЕРНЫЙ
          { id: 8, answer_length: 8, answer: null }, // ИНЦИДЕНТ
          { id: 9, answer_length: 6, answer: 'РЕПОРТ' }, // РЕПОРТ
          { id: 10, answer_length: 6, answer: 'БРОКЕР' }, // БРОКЕР
          { id: 11, answer_length: 3, answer: 'БАГ' }, // БАГ
          { id: 12, answer_length: 5, answer: null }, // АЛЛЮР
          { id: 13, answer_length: 6, answer: null }, // СБОРКА
          { id: 14, answer_length: 6, answer: null } // ОТЧЕТЫ
        ],
        // tasks: [
        //   { id: 1, answer_length: 8, answer: null }, // ПОКРЫТИЕ
        //   { id: 2, answer_length: 8, answer: null }, // СВОЙСТВО
        //   { id: 3, answer_length: 12, answer: null }, // ПОЛЬЗОВАТЕЛЬ
        //   { id: 4, answer_length: 11, answer: null }, // АВТОРИЗАЦИЯ
        //   { id: 5, answer_length: 4, answer: null }, // ПРОД
        //   { id: 6, answer_length: 11, answer: null }, // НАГРУЗОЧНОЕ
        //   { id: 7, answer_length: 6, answer: null }, // ЧЕРНЫЙ
        //   { id: 8, answer_length: 8, answer: null }, // ИНЦИДЕНТ
        //   { id: 9, answer_length: 6, answer: null }, // РЕПОРТ
        //   { id: 10, answer_length: 6, answer: null}, // БРОКЕР
        //   { id: 11, answer_length: 3, answer: null }, // БАГ
        //   { id: 12, answer_length: 5, answer: null }, // АЛЛЮР
        //   { id: 13, answer_length: 6, answer: null }, // СБОРКА
        //   { id: 14, answer_length: 6, answer: null } // ОТЧЕТЫ
        // ],
        time_start: 570,
        started_at: new Date() // ожидаем получить с бека время начала
      })

      // * конец удаления, ниже раскомментировать * //

      // axios.get('/tasks').then(response => {
      //   commit('setTasks', response.data)
      // })
    }
  },
  modules: {
  }
})
